// $Id: 

--Accountexpiry Module--

This module allows creation and maintenance of an account expiry date specific to a user, and depending on their account type. 

Pre-requisite for the account expiry module is the accounttypes module that allows the definition of certain account types, e.g. full member, trial member, etc.

This module (account expiry) allows to define which account types will have an expiry date - e.g. full members don't have an expiry date, but trial members do.

A default expiry date can be defined - e.g. 90 days.

On creation of a new user with an account type that expires, an account expiry record is created in the background with the default account expiry date. This date can then be changed indivdually for each user, either on the account expiry page, or on the user edit page.

Changing an expiry date to a past date blocks a previously active user, changing an expiry date for a blocked user to a future date adtivates this user.

Once an expiry date for a user account has passed, the account is inactivated (blocked) by the next run of the cron job.


The pages you will need to visit to see/invoke all of the functionality are:

--site admin--

	[admin/user/accounttypes - (provided by account types module) - Create and maintain Account Types]

  admin/settings/account_expiry - define which account types have an expiry date, and the default number of days until expiry
  
  admin/user/user/create - on creation of a new user with an account type that expires, an account expiry record is created in the background with the default account expiry date. 
  
  admin/user/user, "edit" a user with an expiring account type: On this page, the default expiry date can be modified individually for this user.
  
  admin/user/account_expiry - this screen lists all users that have an account expiry date, with the option to change any of the expiry dates.
  

Test cases that this module has passed successfully:

* Create a new user w/ account type that does not expire, e.g. full member: no account expiry record is created.
* Create a new user w/ account type that expires, e.g. trial member: account expiry is set according to the defined default expiry days, record is shown on the account expiry screen.
* Change account type from full member to trial member - an account expiry record is created in the background
* Change account type from trial member to full member, for an active user: User is no longer shown on account expiry page.
* Delete a user that is a trial member
* Change Expiry Dates for Trial Members:
  ** Change expiry for active user to any future date: account stays active
  ** Change expiry for active user to past date: account becomes blocked
  ** Change expiry for for blocked user to future date: account becomes active 
  ** change expiry for blocked user to past date: account stays blocked
* Ensure that accounts get blocked once expiry date has passed
* On the settings page: Change default days: no effect on current users, new default days are applied to new users with an expiring account type.

To Do:
* Notification emails to user and/or site administrator, a {configurable period} before expiry, and on expiry.
* Manage installation of the module on an existing site that already has users of an account type that gets an expiry date. Currently each of those users will need to be manually edited, to set an appropriate expiry date.
* Separate default expiry period per account type.
* Allow configuration of action to take on expiry: 'block user' or 'delete user' (?)